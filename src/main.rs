fn print_usage() {
    let usage = "\
usage: gitr <command> [<args>]

These are common Git commands used in various situations:

start a working area
    init    Create an empty Git repository or reinitialize an existing one

work on the current change
    add     Add file contents to the index
    rm      Remove files from the working tree and from the index

examine the history and state
    log     Show commit logs

grow, mark and tweak your common history
    checkout    Switch branches or restore working tree files
    commit      Record changes to the repository
    merge       Join two or more development histories together
    rebase      reapply commits on top of another base tip
    tag         Create, list, delete or verify a tag object
";

    println!("{usage}");
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() < 2 {
        print_usage();
        std::process::exit(1);
    }

    if let Err(e) = gitr::run(&args) {
        eprintln!("Application error: {e}");
        std::process::exit(1);
    }
}

