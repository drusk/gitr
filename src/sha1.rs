//! SHA-1 stands for Secure Hash Algorithm 1
//! It takes an input and produces a 160 bit (20 byte) hash value often
//! called a message digest.
//! <https://en.wikipedia.org/wiki/SHA-1>
//!
//! This implementation is based on RFC3174's C reference implementation:
//! <https://www.rfc-editor.org/rfc/rfc3174>

/// Returns the SHA-1 message digest represented as 40 hexadecimal digits
pub fn sha1_hex(input: &str) -> String {
    String::from("TODO")
}

const SHA1_HASH_SIZE_BYTES: usize = 20;
const MESSAGE_BLOCK_SIZE_BYTES: usize = 64;

struct SHA1 {
    intermediate_hash: [u32; SHA1_HASH_SIZE_BYTES / 4],
    length_low: u32,
    length_high: u32,
    message_block_index: usize,
    message_block: [u8; MESSAGE_BLOCK_SIZE_BYTES],
    computed: bool,
    corrupted: bool,
}

impl SHA1 {
    pub fn new() -> SHA1 {
        SHA1 {
            length_low: 0,
            length_high: 0,
            message_block_index: 0,
            message_block: [0; MESSAGE_BLOCK_SIZE_BYTES],

            intermediate_hash: [0x67452301, 0xEFCDAB89, 0x98BADCFE, 0x10325476, 0xC3D2E1F0],

            computed: false,
            corrupted: false,
        }
    }

    pub fn input(message: Vec<u8>) {
        // TODO implement
    }

    pub fn result(&mut self) -> [u32; SHA1_HASH_SIZE_BYTES] {
        if !self.computed {
            self.pad_message();
            self.clear_message_block();
            self.computed = true;
        }
        let mut result = [
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        ];

        for i in 0..SHA1_HASH_SIZE_BYTES / 4 {
            result[i] = self.intermediate_hash[i >> 2] >> 8 * (3 - (i & 0x03));
        }

        result
    }

    /// Messages must be padded to 512 bits.
    /// The first padding bit must be a 1.
    /// The last 64 bits represent the length of the original message.
    /// All bits in between should be 0.
    fn pad_message(&mut self) {
        if self.message_block_index > 55 {
            self.message_block[self.message_block_index] = 0x80;
            self.message_block_index += 1;
            while self.message_block_index < 64 {
                self.message_block[self.message_block_index] = 0;
                self.message_block_index += 1;
            }

            self.process_message_block();

            while self.message_block_index < 56 {
                self.message_block[self.message_block_index] = 0;
                self.message_block_index += 1;
            }
        } else {
            self.message_block[self.message_block_index] = 0x80;
            while self.message_block_index < 56 {
                self.message_block[self.message_block_index] = 0;
                self.message_block_index += 1;
            }
        }

        self.message_block[56] = (self.length_high >> 24) as u8;
        self.message_block[57] = (self.length_high >> 16) as u8;
        self.message_block[58] = (self.length_high >> 8) as u8;
        self.message_block[59] = self.length_high as u8;

        self.message_block[60] = (self.length_low >> 24) as u8;
        self.message_block[61] = (self.length_low >> 16) as u8;
        self.message_block[62] = (self.length_low >> 8) as u8;
        self.message_block[63] = self.length_low as u8;

        self.process_message_block();
    }

    fn process_message_block(&self) {
        // TODO
    }

    fn clear_message_block(&mut self) {
        for i in 0..MESSAGE_BLOCK_SIZE_BYTES {
            self.message_block[i] = 0;
        }

        self.length_low = 0;
        self.length_high = 0;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn short_word() {
        assert_eq!(
            sha1_hex("test"),
            String::from("a94a8fe5ccb19ba61c4c0873d391e987982fbbd3")
        );
    }

    #[test]
    fn empty_string() {
        assert_eq!(
            sha1_hex(""),
            String::from("da39a3ee5e6b4b0d3255bfef95601890afd80709")
        );
    }

    #[test]
    fn sentence() {
        assert_eq!(
            sha1_hex("The quick brown fox jumps over the lazy dog"),
            String::from("2fd4e1c67a2d28fced849ee1bb76e7391b93eb12")
        );
    }
}
