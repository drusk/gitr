use std::fs::File;
use std::io::Write;
use std::path::{ Path, PathBuf };

pub fn run(args: &Vec<String>) -> Result<(), Box<dyn std::error::Error>> {
    let command = &args[1];

    if command == "init" {
        git_init()?;
    } else {
        println!("Unsupported command: {command}");
    }

    Ok(())
}

fn git_init() -> std::io::Result<()> {
    // For now only support `init`ing a repo in the current directory
    let repo = GitRepository::new();
    repo.create_git_directory("branches");
    repo.create_git_directory("objects");
    repo.create_git_directory("refs/tags");
    repo.create_git_directory("refs/heads");

    let mut head_filehandle = repo.create_git_file("HEAD")?;
    head_filehandle.write(b"ref: refs/heads/maser\n")?;

    let mut description_filehandle = repo.create_git_file("description")?;
    description_filehandle.write(b"Unnamed respository; edit this file 'description' to name the repository.\n")?;

    // TODO probably should refactor this out for clarity
    let mut config_filehandle = repo.create_git_file("config")?;
    config_filehandle.write(b"\
[core]
repositoryformatversion = 0
filemode = false
bare = false\n")?;

    let repo_abs_path = std::fs::canonicalize(repo.git_dir)?;
    println!("Initialized empty Git repository in {}", repo_abs_path.display());

    Ok(())
}

struct GitRepository {
    git_dir: String,
}

impl GitRepository {
    fn new() -> GitRepository {
        let repo = GitRepository {
            git_dir: String::from(".git"),
        };

        repo.create_git_directory("objects");

        repo
    }

    fn git_path(&self, path: &str) -> PathBuf {
        Path::new(&self.git_dir).join(path)
    }

    fn create_git_file(&self, path: &str) -> Result<File, std::io::Error> {
        let path = self.git_path(path);

        let directory = path.parent();
        if let Some(path) = directory {
            if !path.exists() {
                // TODO: figure out when to use PathBuf vs Path
                self.create_git_directory_path(&PathBuf::from(path));
            }
        }

        let new_file = File::create(path)?;

        Ok(new_file)
    }

    fn create_git_directory(&self, path: &str) {
        self.create_git_directory_path(&self.git_path(path));
    }

    fn create_git_directory_path(&self, path: &PathBuf) {
        if path.exists() {
            // TODO should I handle this case differently?
            return;
        }

        // TODO proper error handling
        std::fs::create_dir_all(path).expect("Error creating directory");
    }
}

