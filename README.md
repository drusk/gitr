# gitr

A partial git client implementation written in Rust.

This is purely an exercise for me to practice Rust programming while also
learning some more about git internals.

I followed along with this Python tutorial, implementing in Rust instead:
https://wyag.thb.lt/

# Building

```
cargo build
```

This produces a debug build at `target/debug/gitr`.

# Testing

Run the test suite with the following command:

```
cargo test
```

